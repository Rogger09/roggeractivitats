import java.util.Scanner;

public class activitat20 {

	public static void main(String[] args) {

		int dia, mes, any;
		
		Scanner reader = new Scanner(System.in);
		
		System.out.println("Introduce una fecha");
		System.out.println("Escribe el d�a: ");
		dia = reader.nextInt();
		
		if (dia>31 || dia <= 0) {
			System.out.println("Introduce un d�a correcto.");
		}
		
		System.out.println("Escribe el mes: ");
		mes = reader.nextInt();
		
		if (mes>12 || mes <= 0) {
			System.out.println("Introduce un mes correcto.");
		}
		
		if (mes == 2 && dia > 28) {
			System.out.println("Febrero solo puede tener 28 dias.");
		}
		
		System.out.println("Escribe el a�o.");
		any = reader.nextInt();
					
		if (mes == 2 && dia + 1 > 28 || mes != 2 && dia + 1 > 31) {
			dia = 1;
			mes++;
			
			if (mes > 12) {
				mes = 1;
				any++;
			}				
			
		} else {
			dia++;
		}
		
		System.out.println("Ma�ana sera: " + dia + "/" + mes + "/" + any);
		
		reader.close();
		
	}
}