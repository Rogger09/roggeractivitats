import java.util.Scanner;

public class activitat18 {

	public static void main(String[] args) {

		String qualificacio;
		Scanner scn = new Scanner(System.in);

		System.out.println("Dinos la calificaci�n: ");
		qualificacio = scn.nextLine();

		
		switch (qualificacio.toLowerCase()) {
		
			case "molt deficient":
				System.out.println("1.5");
				break;
				
			case "insuficient":
				System.out.println("4.0");
				break;
				
			case "suficient":
				System.out.println("5.5");
				break;
				
			case "b�":
			case "be":
				System.out.println("6.5");
				break;
				
			case "notable":
				System.out.println("8.0");
				break;
				
			case "excel�lent":
				System.out.println("9.5");
				break;
				
			default:
				System.out.println("Introduce una calificaci�n valida");
		}
		scn.close();
	}

}
