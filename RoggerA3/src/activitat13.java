import java.util.Scanner;

public class activitat13 {

	public static void main(String[] args) {

		int A,B,C;
		double arrel;
		double resultatPos, resultatNeg;
		
		Scanner scn = new Scanner(System.in);
		
		System.out.println("Indica el valor de A:");
		A = scn.nextInt();
		
		System.out.println("Indica el valor de B:");
		B = scn.nextInt();
		
		 System.out.println("Indica el valor de C:");
		C = scn.nextInt();
		
		arrel = B*B - 4 * A * C;
		
		if (arrel > 0) {
			
			arrel = Math.sqrt(arrel);
			
			resultatPos = (-B + arrel) / (2 * A);
			resultatNeg = (-B - arrel) / (2 * A);
			
			System.out.println("Resultat +: " + resultatPos);
			System.out.println("Resultat -: " + resultatNeg);
			
		} else {
			System.out.println("El resultado es negativo...");
		}
		scn.close();
	}

}
