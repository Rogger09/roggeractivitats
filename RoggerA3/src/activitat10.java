import java.util.Scanner;

public class activitat10 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int n1;
		int n2;
		
		Scanner reader = new Scanner(System.in);
		System.out.println("Di el primer n�mero");
		n1 = reader.nextInt();
		
		while (n1 <= 0) {
			System.out.println("El n�mero tiene que ser positivo");
			n1 = reader.nextInt();
		}
		
		System.out.println("Di el segundo n�mero");
		n2 = reader.nextInt();
		
		while (n2 <= 0) {
			System.out.println("El n�mero tiene que ser positivo");
			n2 = reader.nextInt();
		}
		
		if (n1 > n2) {
			if (n1 % n2 == 0) {
				System.out.println("El n�mero " + n1 + " es multiple de " + n2);
			} else {
				System.out.println("El n�mero " + n1 + " no es multiple de " + n2);
			}
		} else {
			if (n2 % n1 == 0) {
				System.out.println("El n�mero " + n2 + " es multiple de " + n1);
			} else {
				System.out.println("El n�mero " + n2 + " no es multiple de " + n1);
			}
		}
        reader.close();
	}
}