import java.util.Scanner;

public class activitat15 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int n1;
		int n2;
		int n3;
		
		Scanner reader = new Scanner(System.in);
		
		System.out.println("Dinos el primer n�mero");
		n1 = reader.nextInt();
		
		System.out.println("Dinos el segundo n�mero");
		n2 = reader.nextInt();
		
		System.out.println("Actualment el primer n�mero es: " + n1);
		System.out.println("Actualment el segundo n�mero es: " + n2);
		
		n3 = n1;
		n1 = n2;
		n2 = n3;
		
		System.out.println("N�meros intercanbiados");
		System.out.println("Actualment el primer n�mero es: " + n1);
		System.out.println("Actualment el segundo n�mero es: " + n2);
		
		reader.close();
	}
	
}