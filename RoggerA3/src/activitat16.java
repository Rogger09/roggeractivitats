import java.util.Scanner;

public class activitat16 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int n1;
		
		Scanner reader = new Scanner(System.in);
		
		System.out.println("Dinos la nota");
		n1 = reader.nextInt();
		
		if (n1 == 5) {
			System.out.println("Tienes un Suficiente");
		} else if (n1 == 6) {
			System.out.println("Tienes un Bien");
		} else if (n1 >= 0 && n1 < 5) {
			System.out.println("Tienes un Insuficiente");
		} else if (n1 >= 7 && n1 < 9) {
			System.out.println("Tienes un Notable");
		} else if (n1 == 9 || n1 == 10) {
			System.out.println("Tienes un Excelente");
		}
		reader.close();		
	}
}