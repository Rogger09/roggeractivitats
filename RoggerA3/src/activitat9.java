import java.util.Scanner;

public class activitat9 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int numero;

		Scanner reader = new Scanner(System.in);
		System.out.println("Di un n�mero");
		numero = reader.nextInt();

		if (numero % 2 == 0 || numero % 3 == 0) {

			if (numero % 2 == 0) {
				System.out.println("El n�mero " + numero + " es multiple de 2");
			}

			if (numero % 3 == 0) {
				System.out.println("El n�mero " + numero + " es multiple de 3");
			}

		} else {
			System.out.println("El n�mero " + numero + " no es multiplo");
		}

		reader.close();
	}
}
