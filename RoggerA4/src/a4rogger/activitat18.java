package a4rogger;

import java.util.Scanner;

public class activitat18 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int pas = 0, espai = 1;
		char lletra;
		String frase;

		Scanner scan = new Scanner(System.in);

		System.out.print("Introdueix la frase: ");

		frase = scan.nextLine();
		lletra = frase.charAt(pas);

		while (lletra != '.') {
			if (lletra == ' ') {
				espai++;
			}
			pas++;
			lletra = frase.charAt(pas);
		}

		System.out.println("La frase t� " + espai + " paraules");

		scan.close();
	}
}
