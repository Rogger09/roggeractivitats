package a4rogger;

import java.util.Scanner;

public class activitat11 {

	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);

		int num;

		System.out.println("Introduce un n�mero");
		num = reader.nextInt();

		for (int i = 1; i <= num; i++) {
			if (num % i == 0) {
				System.out.println("El n�mero " + i + " es divisible de " + num);
			}

		}
		reader.close();
	}
}
