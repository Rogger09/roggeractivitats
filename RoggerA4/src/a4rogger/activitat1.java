package a4rogger;

import java.util.Scanner;

public class activitat1 {

	public static void main(String[] args) {
		
		Scanner reader = new Scanner(System.in);
				
		int num = 0;
		int posi = 0;
		
		for (int i=0;i<10;i++) {
			
			System.out.println("Escribe un n�mero " + (i+1) + ":");
			num = reader.nextInt();
			
			if (num>0) {
				posi++;
			}			
		}
		
		if (posi > 0) {
			System.out.println("Hay un total de: " + posi + " n�meros positivos");
		} else {
			System.out.println("No hay ningun n�mero positivo");
		}
		reader.close();
	}
}
