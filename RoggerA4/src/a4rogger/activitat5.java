package a4rogger;

import java.util.Scanner;

public class activitat5 {

	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);

		int parell = 0;
		int senar = 0;

		for (int i = 1; i <= 100; i++) {
			if (i % 2 == 0) {
				parell = parell + i;
			} else {
				senar = senar + i;
			}
		}
		System.out.println("La suma total de los n�meros pares es: " + parell);
		System.out.println("La suma total de los n�meros senares es: " + senar);

		reader.close();
	}
}
