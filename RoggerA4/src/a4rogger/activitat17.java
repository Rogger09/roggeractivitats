package a4rogger;

import java.util.Scanner;

public class activitat17 {

	public static void main(String[] args) {

		int caracters = 0, frases = 0, pas = 0;
		char lletra;
		String frase;

		Scanner scan = new Scanner(System.in);

		System.out.println("Si vols acabar escriu: $");
		System.out.print("Introdueix les frases:  ");
		frase = scan.nextLine();

		lletra = frase.charAt(pas);

		while (lletra != '$') {
			frases++;
			caracters = frase.length();

			System.out.println("La frase t� " + caracters + " car�rcters");
			System.out.println("\n" + "Si vols acabar escriu: $");
			System.out.print("Introdueix la frase: ");

			frase = scan.nextLine();
			lletra = frase.charAt(0);
		}

		System.out.println("El n�mero de frases introdu�des s�n " + frases);

		scan.close();
	}
}
