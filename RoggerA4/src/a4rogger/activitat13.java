package a4rogger;

import java.util.Scanner;

public class activitat13 {

	public static void main(String[] args) {
		
		int num = 0, mitjana = 0, notes = -1;
		int excelent = 0, notable = 0, be = 0, suficient = 0, insuficient = 0, deficient = 0;

		Scanner scan = new Scanner(System.in);

		while (num != -1) {

			System.out.print("Nota: ");
			num = scan.nextInt();

			if (num != -1) {

				switch (num) {
				case 0:
				case 1:
				case 2:
					mitjana = mitjana + num;
					deficient++;
					notes++;
					break;
				case 3:
				case 4:
					mitjana = mitjana + num;
					insuficient++;
					notes++;
					break;
				case 5:
				case 6:
					mitjana = mitjana + num;
					be++;
					notes++;
					break;
				case 7:
				case 8:
				case 9:
					mitjana = mitjana + num;
					notable++;
					notes++;
					break;
				case 10:
					mitjana = mitjana + num;
					excelent++;
					notes++;
					break;

				default:
					System.out.println("El n�mero no hi �s al rang de 0 a 10");
					break;
				}

			}

		}

		System.out.println("\n" + "Excel�lent: " + excelent);
		System.out.println("Notable: " + notable);
		System.out.println("B�: " + be);
		System.out.println("Suficient: " + suficient);
		System.out.println("Insuficient: " + insuficient);
		System.out.println("Molt deficient: " + deficient);

		System.out.println("\n" + "La mitjana aritm�tica �s " + mitjana / notes);

		scan.close();

	}

}
