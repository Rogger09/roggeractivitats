package a4rogger;

import java.util.Scanner;

public class activitat6 {

	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);

		int num;
		int positius = 0;
		int negatius = 0;
		int zero = 0;

		for (int i = 0;i<10;i++) {

			System.out.println("Dinos el n�mero " + (i + 1) + ":");
			num = reader.nextInt();

			if (num > 0) {
				positius++;
			}
			if (num < 0) {
				negatius++;
			}
			if (num == 0) {
				zero++;
			}
		}
		System.out.println("Hay un total de: " + positius + " n�meros positius");
		System.out.println("Hay un total de: " + negatius + " n�meros negatius");
		System.out.println("Hay un total de: " + zero + " n�meros zero");
		
		reader.close();
	}
}