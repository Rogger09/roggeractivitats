package a4rogger;

import java.util.Scanner;

public class activitat0b {

	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);

		int casos;
		int valor;
		int mesGran = 0;
		int posicioMesGran = 0;
		int num = 0;

		casos = reader.nextInt();

		for (int i = 1; i <= casos; i++) {
			valor = reader.nextInt();
			mesGran = 0;
			posicioMesGran = 0;
			for (int y = 1; y <= valor; y++) {
				num = reader.nextInt();
				if (num > mesGran) {
					mesGran = num;
					posicioMesGran = y;
				}
			}
			System.out.println(posicioMesGran);
		}
		reader.close();
	}
}