package a4rogger;

import java.util.Scanner;

public class activitat7 {

	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);

		int num;
		int mesgran = 0;

		for (int i = 0;; i++) {

			System.out.println("Dinos un n�mero " + (i + 1) + ":");
			num = reader.nextInt();

			if (num == 0) {
				System.out.println("El n�mero mas grande es: " + mesgran);
				break;
			}

			if (num >= mesgran) {
				mesgran = num;
			}

			if (num < 0) {
				System.out.println("Inserta un n�mero superior a 0");
			}
		}
		reader.close();
	}
}