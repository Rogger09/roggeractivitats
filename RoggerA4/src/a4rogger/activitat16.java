package a4rogger;

import java.util.Scanner;

public class activitat16 {

	public static void main(String[] args) {

		int count = 0, pas = 0;
		char lletra;
		String frase;

		Scanner scan = new Scanner(System.in);

		System.out.print("Introdueix una frase acabada en punt: ");
		frase = scan.nextLine();

		lletra = frase.charAt(pas);

		while (lletra != '.') {

			if (lletra == 'l' || lletra == 'L') {
				pas++;
				lletra = frase.charAt(pas);
				if (lletra == 'a' || lletra == 'A') {
					count++;
				}

			} else {
				pas++;
				lletra = frase.charAt(pas);
			}

		}
		System.out.println("La silaba LA surt " + count + " cops");

		scan.close();
	}
}