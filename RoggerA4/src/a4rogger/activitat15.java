package a4rogger;

import java.util.Scanner;

public class activitat15 {

	public static void main(String[] args) {

		int count = 0, pos = 0;
		char lletra;
		String frase;

		Scanner scan = new Scanner(System.in);

		System.out.print("Introdueix una frase acabada en punt: ");
		frase = scan.nextLine();

		lletra = frase.charAt(0);

		while (lletra != '.') {

			switch (lletra) {
			case 'a':
			case 'A':
			case 'e':
			case 'E':
			case 'i':
			case 'I':
			case 'o':
			case 'O':
			case 'u':
			case 'U':
				count++;
				break;

			default:
				break;
			}

			pos++;

			lletra = frase.charAt(pos);
		}

		System.out.println("El n�mero de vegades que ha sortit la lletra A �s: " + count);

		scan.close();

	}

}
