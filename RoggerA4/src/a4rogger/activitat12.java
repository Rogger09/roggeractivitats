package a4rogger;

import java.util.Scanner;

public class activitat12 {

	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);

		int num;
		int mesgran = 0;
		int mespetit = 10;
		double mitjana = 0;

		for (int i = 0; i < 10; i++) {
			System.out.println("Introduce un n�mero " + i);
			num = reader.nextInt();
			
			if(num > mesgran) {
				mesgran = num;
			}
			
			if(num < mespetit) {
				mespetit = num;
			}
			
			mitjana = mitjana + num;
		}
		System.out.println("El n�mero mes gran es: " + mesgran);
		System.out.println("El n�mero mes petit es: " + mespetit);
		System.out.println("La mitjana es: " + (mitjana / 10));
		
		reader.close();
	}
}
