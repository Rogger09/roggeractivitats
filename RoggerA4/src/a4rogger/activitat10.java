package a4rogger;

import java.util.Scanner;

public class activitat10 {

	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);

			int num1;
			int num2;
			int prod = 1;
			
			System.out.println("Digues un n�mero");
			num1 = reader.nextInt();
			
			System.out.println("Digues un n�mero");
			num2 = reader.nextInt();
			
			for (int i=0;i<num2;i++) {
				System.out.println(prod + " * " + num1 + " = " + (prod*num1));
				prod *= num1;
			}
			
			System.out.println(num1 + "^" + num2 + " = " + prod);
			reader.close();
	}
}
